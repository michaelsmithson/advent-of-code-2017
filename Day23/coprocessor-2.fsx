let prime b = seq { 2..(b-1) } |> Seq.exists (fun d -> b % d = 0) |> not

let part2 = [106700..17..123700] |> List.filter (not << prime) |> List.length

// debug values of b/c
set b 67
set c b
jnz a 2 
jnz 1 5
// not debug values of b/c
mul b 100
sub b -100000
set c b
sub c -17000

b = 67 * 100 - (-100000) = 106700
c = 106700 - (-17000) = 123700
// end notdebug

f = 1
set f 1
loop3:
set d 2

loop1:
set e 2
loop2:
set g d
mul g e
sub g b
jnz g 2
set f 0
sub e -1
set g e
sub g b
jnz g -8 // loop2
sub d -1
set g d
sub g b
jnz g -13 // loop1


for (d = 2 ; d < b; d++)
    for ( e = 2; e < b; e++)            
        //g = e * d
        //g = g - b
        if e * d = b then f = 0
    
jnz f 2
sub h -1

if f = 1 then h = h + 1

set g b
sub g c // if g = c then exit
jnz g 2
jnz 1 3  // exit
if g = c then exit

sub b -17
b = b + 17

jnz 1 -23 // loop3



