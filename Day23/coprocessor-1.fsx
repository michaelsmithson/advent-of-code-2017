
type Register = char

type RegisterOrConstant = | Register of Register | Constant of int64 

type Instruction =
    | Set of Register * RegisterOrConstant
    | Sub of Register * RegisterOrConstant
    | Mul of Register * RegisterOrConstant
    | Jnz of RegisterOrConstant * RegisterOrConstant

type State = { registers: Map<Register,int64>; instructions: Instruction[]; currentInstruction: int;  mulCount: int; operationCount: int }

type ProcessState =
    | Running of State
    | Terminated of State

let parseValue token =
    let (valid,value) = System.Int64.TryParse token
    if valid then Constant value else Register token.[0]

let parseInstruction (s:string) = 
    let tokens = s.Split([|' '|])
    let registerAndInt() = (tokens.[1].[0], parseValue tokens.[2]) 
    match tokens.[0] with
    | "set" -> Set <| registerAndInt()
    | "sub" -> Sub <| registerAndInt()
    | "mul" -> Mul <| registerAndInt()
    | "jnz" -> Jnz (parseValue tokens.[1], parseValue tokens.[2])
    | _ -> failwith "unsupported instruction"

let getRegisterValue register registers = 
    Map.tryFind register registers 
    |> Option.defaultValue 0L 

let evaluate value registers =
    match value with 
    | Constant v -> v
    | Register r -> getRegisterValue r registers

let setRegister r v state = { state with registers = Map.add r v state.registers; currentInstruction = state.currentInstruction + 1; operationCount = state.operationCount + 1 }

let incMulCount s = {s with mulCount = s.mulCount + 1}

let processInstruction instruction state =
    match instruction with
    | Set (r, v) ->
        let y = evaluate v state.registers
        setRegister r y state
    | Sub (r, v) ->
        let x = getRegisterValue r state.registers
        let y = evaluate v state.registers
        setRegister r (x - y) state
    | Mul (r, v) ->
        let x = getRegisterValue r state.registers
        let y = evaluate v state.registers
        state |> setRegister r (x * y) |> incMulCount
    | Jnz (r, v) ->
        let x = evaluate r state.registers
        if x <> 0L then
            let y = evaluate v state.registers
            { state with currentInstruction = state.currentInstruction + int y; operationCount = state.operationCount + 1 }
        else
            { state with currentInstruction = state.currentInstruction + 1; operationCount = state.operationCount + 1 }


let processState state =
    match Array.tryItem state.currentInstruction state.instructions with
    | None -> Terminated state
    | Some instruction -> processInstruction instruction state |> Running

let rec run state =
    match processState state with
    | Terminated x -> x
    | Running state -> run state   

let initialState instructions initialA = {registers = Map.add 'a' initialA  Map.empty; instructions = instructions; currentInstruction = 0; mulCount = 0; operationCount = 0}

System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
let instructions = System.IO.File.ReadAllLines("input.txt") |> Array.map parseInstruction
let part1 = 
    initialState instructions 0L
    |> run
    |> fun state -> state.mulCount