#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto


let findMaxBlock input = input |> Seq.indexed |> Seq.maxBy snd

let distribute banks (maxIndex,maxValue) = 
    let result = Array.copy banks
    let length = Array.length result
    result.[maxIndex] <- 0
    
    let rec loop nextIndex remaining =
        if remaining = 0 then result
        else
            let i = if nextIndex >= length then 0 else nextIndex
            result.[i] <- result.[i] + 1
            loop (i + 1) (remaining - 1)
    loop (maxIndex + 1) maxValue

let createBankString banks = banks |> Array.fold (fun acc block -> acc + (sprintf "%i|" block)) ""

let solve banks = 
        let rec loop banks previousStates iterations =
            let nextBanks = findMaxBlock banks |> distribute banks
            let nextString = createBankString nextBanks
            match Map.tryFind nextString previousStates with
            | Some prevIterations -> iterations + 1, iterations - prevIterations 
            | None ->
                loop nextBanks (Map.add nextString iterations previousStates) (iterations + 1)
        loop banks Map.empty 0

let input = "10 3 15 10 5 15 5 15 9 2 5 8 5 2 3 6"

let part1,part2 = input.Split() |> Array.map int |> solve

let tests = testList "Sample" [
    testCase "Distribute" <| fun _ ->
        let result = distribute [|0;2;7;0|] (2, 7)
        Expect.sequenceEqual result [|2;4;1;2|] "Sample first distribution"
    testCase "Part 1" <| fun _ ->
        let sample = [|0;2;7;0|]
        let result = solve sample |> fst
        Expect.equal result 5 "Part 1"
    testCase "Part 2" <| fun _ ->
        let sample = [|0;2;7;0|]
        let result = solve sample |> snd
        Expect.equal result 4 "Part 2"
]

tests |> runTests defaultConfig