
module Knothash = 
    let private rotate i a = 
        let n = Array.length a
        [|for k in 0..n - 1 ->
                let k = (k + i) % n
                a.[if k < 0 then n + k else k] |]

    let private reverse input position length =
            let rotated = rotate position input
            let last = rotated |> Array.skip length
            let start = rotated |> Array.take length |> Array.rev
            
            Array.concat [start; last] 
            |> rotate (-position)

    let rec private processRound lengths skipSize position input =
            let inputLength = Array.length input
            match lengths with
            | [] -> input
            | length::t ->
                    let updated = reverse input position length 
                    let nextPosition = (position + length + skipSize) % inputLength
                    processRound t (skipSize + 1) nextPosition updated

    let private processRounds count lengths input =
            // since I don't want to change the part 1 algorithm now we have part 2... and we have to 
            // track the position and skipsize, lets just make the length longer
            let multipleRounds = List.concat <| seq {for _ in 1..count do yield lengths}
            processRound multipleRounds 0 0 input

    let private xorArray a = a |> Array.reduce (^^^)

    let private hexString (i:int) = i.ToString("X2").ToLowerInvariant()

    let private generateLengths (input:string) =
            let additionalLengths = [|17;31;73;47;23|]
            let inputLengths = input.ToCharArray() |> Array.map int
            Array.concat [inputLengths; additionalLengths] |> List.ofArray

    let private sparseHash input = 
            let lengths = generateLengths input
            processRounds 64 lengths [|0..255|]

    let private denseHash sparse =
            sparse
            |> Array.splitInto 16
            |> Array.map (xorArray >> hexString)
            |> System.String.Concat

    let knothash = sparseHash >> denseHash

let hexCharToBinary (c:char) = 
            let i = System.Convert.ToInt32(c.ToString(), 16)
            System.Convert.ToString(i, 2).PadLeft(4, '0')

let hexToBinary (h:string) = h.ToCharArray() |> Array.map hexCharToBinary |> System.String.Concat

let countOnes s = s |> String.filter (fun c -> c = '1') |> String.length

let createGrid key = 
            let rowInput n = sprintf "%s-%i" key n 
            [|0..127|]
            |> Array.map (rowInput >> Knothash.knothash >> hexToBinary >> (fun x -> x.ToCharArray()))

let solve1 key =
            let countRow row = row |> Seq.filter ((=) '1') |> Seq.length
            createGrid key
            |> Array.sumBy countRow

//////////////////////////////////////////////////////////////////////////////

type Cell = {x:int; y: int}

let value grid cell =
    let row = Array.item cell.x grid
    Array.item cell.y row

let inBounds max cell = cell.x >= 0 && cell.y >= 0 && cell.x < max && cell.y < max 

let neighbours grid cell =
        let max = Array.length grid
        [
            {cell with x = cell.x + 1}
            {cell with x = cell.x - 1}
            {cell with y = cell.y + 1}
            {cell with y = cell.y - 1}
        ]
        |> List.filter (inBounds max)

let exploreRegion cell grid =
    let rec loop toVisit region =
        let notContains cell = not <| Set.contains cell region
        match toVisit with
        | [] -> region
        | cell::t ->
            if value grid cell = '1' && notContains cell then
                let updatedRegion = Set.add cell region
                let unexploredNeighbours = neighbours grid cell |> List.filter notContains
                loop (List.append t unexploredNeighbours) updatedRegion
            else loop t region
    loop [cell] Set.empty
    
let countRegions grid =
    let width = Array.length grid
    let maxI = width * width 
    let rec loop i allRegions regionCount =
        if i >= maxI then regionCount
        else
            let cell = {x = i / width; y = i % width }
            if Set.contains cell allRegions
            then loop (i + 1) allRegions regionCount
            else
                let region = exploreRegion cell grid
                if Set.isEmpty region then loop (i + 1) allRegions regionCount
                else loop (i + 1) (Set.union allRegions region) (regionCount + 1)
    loop 0 Set.empty 0

let solve2 input =
    let grid = createGrid input
    countRegions grid

let part1 = solve1 "amgozmfv"
let part2 = solve2 "amgozmfv"

/////////////////////////////////////////////////////////////////
// Tests

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let hexToBinaryTest (input,expected) =
    testCase input <| fun _ ->
        let result = hexToBinary input
        Expect.equal result expected ""

let hexTests = 
    [   
        ("0", "0000")
        ("f", "1111")
        ("1", "0001")
        ("a0c2017", "1010000011000010000000010111")
    ] 
    |> List.map hexToBinaryTest
    |> testList "hex to binary tests"

let regionTests = 
    let grid = [|"110".ToCharArray();"101".ToCharArray();"001".ToCharArray()|]
    let grid2 = [|"111".ToCharArray();"111".ToCharArray();"111".ToCharArray()|]
    testList "Region" [
        testCase "Simple Region" <| fun _ ->
            let region = exploreRegion {x=0;y=0} grid
            Expect.equal region.Count 3 "Three elements"
            Expect.all [{x=0;y=0}; {x=0;y=1}; {x=1;y=0}] (fun c -> Set.contains c region) ""
        testCase "Block region" <| fun _ ->
            let region = exploreRegion {x=0;y=0} grid2
            Expect.equal region.Count 9 "all elements"
        testCase "No region" <| fun _ ->
            let region = exploreRegion {x=1;y=1} grid
            Expect.equal region.Count 0 "No elements"
        testCase "" <| fun _ ->
            let result = countRegions grid
            Expect.equal result 2 "" 
    ] 

let tests = 
    testList "All tests" [
        hexTests
        regionTests
        testCase "Count ones" <| fun _ ->
            let result = countOnes "001101011"
            Expect.equal result 5 "" 
        testCase "Part 1 Example" <| fun _ ->
            let result = solve1 "flqrgnkx"
            Expect.equal result 8108 "Used 8108 squares"
        testCase "Part 2 Example" <| fun _ ->
            let result = solve2 "flqrgnkx"
            Expect.equal result 1242 "1242 regions expected" 
    ]

tests |> runTests defaultConfig
