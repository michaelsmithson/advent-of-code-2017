
let parse (line:string) = 
        let tokens = line.Replace(",", "").Split([|' '|])
        let program = tokens.[0] |> int
        let connectedPrograms = tokens |> Array.skip 2 |> Array.map int |> List.ofArray
        program, connectedPrograms


let rec extractGroup toProcess allPrograms currentGroup = 
    let alreadyConnected program = Set.contains program currentGroup
    match toProcess with
    | [] -> currentGroup
    | program::t ->            
            let nextToProcess = 
                        allPrograms
                        |> Map.find program 
                        |> List.filter (not << alreadyConnected)
                        |> List.append t
            extractGroup nextToProcess allPrograms (Set.add program currentGroup)

let groupByFirstProgram input =
    let allPrograms = input |> Map.ofArray
    let initial = fst input.[0]
    extractGroup [initial] allPrograms Set.empty
 
let solve1 input = input |> Array.map parse |> groupByFirstProgram |> Set.count

let solve2 input = 
    let rec loop programs groups =
        if Array.isEmpty programs then groups
        else
            let group = groupByFirstProgram programs
            let remaining = programs |> Array.filter (fun (p,_) -> not <| Set.contains p group)
            loop remaining (group::groups)
    let programs = input |> Array.map parse
    let groups = loop programs []
    List.length groups


System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
let input = System.IO.File.ReadAllLines "input.txt"
let part1 = solve1 input

let part2 = solve2 input


/////////////////////////////////////////////////////////////////
// Tests

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let tests = 
    let sample = 
        [|
        "0 <-> 2"
        "1 <-> 1"
        "2 <-> 0, 3, 4"
        "3 <-> 2, 4"
        "4 <-> 2, 3, 6"
        "5 <-> 6"
        "6 <-> 4, 5"
        |]
    testList "All tests" [
        testCase "Example 1" <| fun _ ->
            let result = solve1 sample
            Expect.equal result 6 ""
        testCase "Example 2" <| fun _ ->
            let result = solve2 sample
            Expect.equal result 2 ""
    ]

tests |> runTests defaultConfig