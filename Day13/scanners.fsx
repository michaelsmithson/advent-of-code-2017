
type Layer = { depth: int; range: int }

let parse (s:string) = 
    let token = s.Split([|':'|])
    { depth = int token.[0]; range = int token.[1] }

let isCaptured delay layer = 
    let freq = (layer.range - 1) * 2
    (delay + layer.depth) % freq = 0

let score layer = layer.depth * layer.range

let solve1 input = 
    input
    |> Array.map parse
    |> Array.filter (isCaptured 0)
    |> Array.sumBy score

let solve2 input = 
    let parsed = input |> Array.map parse
    let rec findNoCapture delay =
        if not <| Array.exists (isCaptured delay) parsed 
        then delay
        else findNoCapture (delay + 1)
    findNoCapture 0

System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
let input = System.IO.File.ReadAllLines "input.txt"
let part1 = solve1 input
let part2 = solve2 input


/////////////////////////////////////////////////////////////////
// Tests

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let capturedTests = 
        let captureTest (layer, delay, captured) = 
            testCase (sprintf "depth %i range %i delay %i" layer.depth layer.range delay) <| fun _ ->
                let result = isCaptured delay layer
                Expect.equal result captured ""
        [
            ({depth = 0; range = 3}, 0, true)
            ({depth = 1; range = 2}, 0, false)
            ({depth = 4; range = 4}, 0, false)
            ({depth = 6; range = 4}, 0, true)
            ({depth = 0; range = 3}, 1, false)
            ({depth = 1; range = 2}, 1, true)
            ({depth = 4; range = 4}, 1, false)
            ({depth = 4; range = 4}, 2, true)
            ({depth = 4; range = 4}, 3, false)
            ({depth = 4; range = 4}, 4, false)
            ({depth = 4; range = 4}, 5, false)
            ({depth = 4; range = 4}, 6, false)
            ({depth = 4; range = 4}, 7, false)
            ({depth = 4; range = 4}, 8, true)
            ({depth = 4; range = 4}, 9, false)
            ({depth = 6; range = 4}, 6, true)
            ({depth = 6; range = 4}, 3, false)
        ]
        |> List.map captureTest
        |> testList "Capture"


let tests = 
    let sample = 
        [|
            "0: 3"
            "1: 2"
            "4: 4"
            "6: 4"
        |]

    testList "AllTests" [
        testCase "Parse" <| fun _ ->
            let result = parse "0: 3"
            Expect.equal result {depth = 0; range = 3} ""
        capturedTests
        testCase "Part 1" <| fun _ ->
            let result = solve1 sample
            Expect.equal result 24 ""
        testCase "Part 2" <| fun _ ->
            let result = solve2 sample
            Expect.equal result 10 ""
    ]

tests |> runTests defaultConfig