let sqr x = x * x
let sideLength r = 2 * r + 1
let maxRingValue = sideLength >> sqr

let findRadius n = 
        let rec search r =
            if maxRingValue r >= n then r
            else search (r + 1)
        search 0

let steps n = 
        let r = findRadius n
        let max = maxRingValue r
        let side = sideLength r
        let rec findNearestCorner c =
                let next = c - (side - 1)
                if next <= n then c
                else findNearestCorner next
        let nearestCorner = findNearestCorner max
        let center = nearestCorner - (side / 2)
        let offset = abs (center - n)
        offset + r


let part1 = steps 325489


// Part 2
type Cell = { x: int; y: int }

type Cells = Map<Cell, int>

let getNeighbours cell = [
            {x = cell.x + 1; y = cell.y }
            {x = cell.x + 1; y = cell.y + 1 }
            {x = cell.x + 1; y = cell.y - 1 }
            {x = cell.x - 1; y = cell.y }
            {x = cell.x - 1; y = cell.y + 1 }
            {x = cell.x - 1; y = cell.y - 1 }
            {x = cell.x; y = cell.y + 1 }
            {x = cell.x; y = cell.y - 1 }
        ]

let neighbourSum cell (cells:Cells) = 
        let tryFindNeighbour cell = Map.tryFind cell cells
        cell
        |> getNeighbours
        |> List.map tryFindNeighbour
        |> List.choose id
        |> List.sum

let nextRingPath prev =
        let side = sideLength (prev.x + 1)
        seq { 
            // right 1
            yield {x = prev.x + 1; y = prev.y }
            // up
            for y in 1..(side - 2) do yield {x = prev.x + 1; y = prev.y + y}
            // left
            for x in 1..(side - 1) do yield { x = prev.x + 1 - x; y = prev.y + side - 2 }
            // down
            for y in 1..(side - 1) do yield { x = prev.x + 1 - (side - 1); y = prev.y + (side - 2) - y }
            // right
            for x in (side - 2).. -1 ..0 do yield { x = prev.x + 1 - x; y = prev.y - 1 }
        }

let findFirstLarger n =
    let rec search toProcess cells =
            match toProcess with
            | h::t ->
                    let sum = neighbourSum h cells
                    if sum > n then sum
                    else
                        let next = 
                            match t with 
                            | [] -> nextRingPath h |> List.ofSeq 
                            | _ -> t
                        search next (Map.add h sum cells)
            | _ -> failwith "Unable to find"
    let first = {x = 0; y = 0}
    let initialMap = Map.add first 1 Map.empty 
    let initialSeq = nextRingPath first |> List.ofSeq
    search initialSeq initialMap
    
let part2 = findFirstLarger 325489

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let test n expected = 
    testCase (sprintf "Input: %i" n) <| fun _ ->
        let result = steps n
        Expect.equal result expected ""

let sampleTests = testList "Sample" [
                test 1 0
                test 12 3
                test 23 2
                test 1024 31
                testCase "Part 2" <| fun _ ->
                    let result = findFirstLarger 800
                    Expect.equal result 806 "First after 800 is 806"
                ]
let maxRingTests =                 
    [(0, 1); (1, 9); (2, 25)]
    |> List.map (fun (r, expected) -> 
        testCase (sprintf "Max value for ring %i" r) <| fun _ -> 
            let result = maxRingValue r
            Expect.equal result expected "Expected max ring value")
    |> testList "Max Ring Values"

let findRadiusTests =                 
    [(1, 0); (2, 1); (9, 1); (10, 2); (26, 3)]
    |> List.map (fun (n, expected) -> 
        testCase (sprintf "Radius for number %i" n) <| fun _ -> 
            let result = findRadius n
            Expect.equal result expected "Expected radius value")
    |> testList "Radius"

let nextRingPathTests = 
    testList "RingPath" [
        testCase "First ring" <| fun _ ->
            let result = nextRingPath {x = 0; y = 0} |> List.ofSeq
            let expected = [{x = 1; y = 0}; {x = 1; y = 1}; {x = 0; y = 1}; {x = -1; y = 1};{x = -1; y = 0}; {x = -1; y = -1};{x = 0; y = -1}; {x = 1; y = -1}]
            Expect.sequenceEqual result expected "Neighbours match"
            
    ]

let tests = testList "All tests" [sampleTests; maxRingTests; findRadiusTests; nextRingPathTests ]

tests |> runTests defaultConfig
