
type Register = char

type RegisterOrConstant = | Register of Register | Constant of int64 

type Instruction =
    | Snd of RegisterOrConstant
    | Set of Register * RegisterOrConstant
    | Add of Register * RegisterOrConstant
    | Mul of Register * RegisterOrConstant
    | Mod of Register * RegisterOrConstant
    | Rcv of Register
    | Jgz of RegisterOrConstant * RegisterOrConstant

type State = { registers: Map<Register, int64>; instructions: Instruction[]; currentInstruction: int; lastSound: int64 option }

type ProcessResult = 
    | Running of State
    | Terminated
    | Part1Result of int64 option

let parseValue token =
    let (valid,value) = System.Int64.TryParse token
    if valid then Constant value else Register token.[0]

let parseInstruction (s:string) = 
    let tokens = s.Split([|' '|])
    let registerAndInt() = (tokens.[1].[0], parseValue tokens.[2]) 
    match tokens.[0] with
    | "snd" -> Snd (parseValue tokens.[1])
    | "rcv" -> Rcv tokens.[1].[0]
    | "set" -> Set <| registerAndInt()
    | "add" -> Add <| registerAndInt()
    | "mul" -> Mul <| registerAndInt()
    | "mod" -> Mod <| registerAndInt()
    | "jgz" -> Jgz (parseValue tokens.[1], parseValue tokens.[2])
    | _ -> failwith "unsupported instruction"

let getRegisterValue register registers = 
        match Map.tryFind register registers with
        | None -> 0L
        | Some value -> value

let setRegister r v state = 
    Running { state with 
                registers = Map.add r v state.registers; 
                currentInstruction = state.currentInstruction + 1 }

let evaluate value registers =
    match value with 
    | Constant v -> v
    | Register r -> getRegisterValue r registers

let processInstruction instruction state = 
    match instruction with
    | Snd v -> 
                let x = evaluate v state.registers
                Running { state with lastSound = Some x; currentInstruction = state.currentInstruction + 1 }
    | Rcv r -> 
                let current = getRegisterValue r state.registers
                if current <> 0L then Part1Result state.lastSound
                else Running { state with currentInstruction = state.currentInstruction + 1 }
    | Set (r,v) -> 
                let y = evaluate v state.registers
                setRegister r y state
    | Add (r, v) ->
                let x = getRegisterValue r state.registers
                let y = evaluate v state.registers
                setRegister r (x + y) state
    | Mul (r, v) ->
                let x = getRegisterValue r state.registers
                let y = evaluate v state.registers
                setRegister r (x * y) state
    | Mod (r, v) ->
                let x = getRegisterValue r state.registers
                let y = evaluate v state.registers
                setRegister r (x % y) state
    | Jgz (r, v) ->
                let x = evaluate r state.registers
                if x > 0L then 
                    let y = evaluate v state.registers
                    Running {state with currentInstruction = state.currentInstruction + int y }
                else Running { state with currentInstruction = state.currentInstruction + 1 }

let processState state =
    match Array.tryItem state.currentInstruction state.instructions with
    | None -> Terminated
    | Some instruction -> processInstruction instruction state

                  
let rec run state =
    match processState state with
    | Terminated -> None
    | Part1Result x -> x
    | Running state -> run state   

System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
let instructions = System.IO.File.ReadAllLines("input.txt") |> Array.map parseInstruction

let initialState instructions = {registers = Map.empty; instructions = instructions; currentInstruction = 0; lastSound = None}

let part1 = run <| initialState instructions

////////////////////////////////////////////////////////////////////////////////////////
// Tests

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let sampleInstructions = 
    [|
        "set a 1"
        "add a 2"
        "mul a a"
        "mod a 5"
        "snd a"
        "set a 0"
        "rcv a"
        "jgz a -1"
        "set a 1"
        "jgz a -2"
    |]

let tests = 
    testList "Example" [
        testCase "Part 1" <| fun _ ->
            let initial = sampleInstructions |> Array.map parseInstruction |> initialState
            let result = run initial
            Expect.equal result (Some 4L) "Last freq is 4"
    ]

tests |> runTests defaultConfig