let flip f a b = f b a

type queue<'a> = | Queue of 'a list * 'a list

module Queue = 
    let empty = Queue([], [])

    let isEmpty q = q = empty

    let enqueue e = function
        | Queue(fs, bs) -> Queue(e :: fs, bs)
    let enqueueSeq s q = Seq.fold (flip enqueue) q s

    let dequeue = function
        | Queue([], []) -> failwith "Empty queue!"
        | Queue(fs, b :: bs) -> b, Queue(fs, bs)
        | Queue(fs, []) -> 
            let bs = List.rev fs
            bs.Head, Queue([], bs.Tail)
    
    let tryDequeue q = 
        if isEmpty q then None
        else Some <| dequeue q


type Register = char

type RegisterOrConstant = | Register of Register | Constant of int64 

type Instruction =
    | Snd of RegisterOrConstant
    | SetReg of Register * RegisterOrConstant
    | Add of Register * RegisterOrConstant
    | Mul of Register * RegisterOrConstant
    | Mod of Register * RegisterOrConstant
    | Rcv of Register
    | Jgz of RegisterOrConstant * RegisterOrConstant

type Program = { id: int; registers: Map<Register, int64>; instructions: Instruction[]; currentInstruction: int; sentCount: int; output: int64 list; input: int64 queue }

type State = 
    | Running of Program
    | Terminated
    | Waiting of Program

let parseValue token =
    let (valid,value) = System.Int64.TryParse token
    if valid then Constant value else Register token.[0]

let parseInstruction (s:string) = 
    let tokens = s.Split([|' '|])
    let registerAndInt() = (tokens.[1].[0], parseValue tokens.[2]) 
    match tokens.[0] with
    | "snd" -> Snd (parseValue tokens.[1])
    | "rcv" -> Rcv tokens.[1].[0]
    | "set" -> SetReg <| registerAndInt()
    | "add" -> Add <| registerAndInt()
    | "mul" -> Mul <| registerAndInt()
    | "mod" -> Mod <| registerAndInt()
    | "jgz" -> Jgz (parseValue tokens.[1], parseValue tokens.[2])
    | _ -> failwith "unsupported instruction"

let getRegisterValue register registers = 
        match Map.tryFind register registers with
        | None -> 0L
        | Some value -> value

let setRegister r v state = 
    Running { state with 
                registers = Map.add r v state.registers; 
                currentInstruction = state.currentInstruction + 1 }

let evaluate value registers =
    match value with 
    | Constant v -> v
    | Register r -> getRegisterValue r registers

let applyInstruction instruction state = 
    match instruction with
    | Snd v -> 
                let x = evaluate v state.registers
                Running { state with sentCount = state.sentCount + 1; output = x::state.output; currentInstruction = state.currentInstruction + 1 }
    | Rcv r -> 
                match Queue.tryDequeue state.input with
                | None -> Waiting state
                | Some (e, q) -> 
                        Running { state with
                                    registers = Map.add r e state.registers;
                                    currentInstruction = state.currentInstruction + 1;
                                    input = q }
    | SetReg (r,v) -> 
                let y = evaluate v state.registers
                setRegister r y state
    | Add (r, v) ->
                let x = getRegisterValue r state.registers
                let y = evaluate v state.registers
                setRegister r (x + y) state
    | Mul (r, v) ->
                let x = getRegisterValue r state.registers
                let y = evaluate v state.registers
                setRegister r (x * y) state
    | Mod (r, v) ->
                let x = getRegisterValue r state.registers
                let y = evaluate v state.registers
                setRegister r (x % y) state
    | Jgz (r, v) ->
                let x = evaluate r state.registers
                if x > 0L then 
                    let y = evaluate v state.registers
                    Running {state with currentInstruction = state.currentInstruction + int y }
                else Running { state with currentInstruction = state.currentInstruction + 1 }

let processInstruction program =
    match Array.tryItem program.currentInstruction program.instructions with
    | None -> Terminated
    | Some instruction -> applyInstruction instruction program
                  
let rec runProgram program =
    match processInstruction program with
    | Terminated -> Terminated
    | Waiting p -> Waiting p
    | Running state -> runProgram state   

let isBlocked program = program.input = Queue.empty

let rec run foreground background =
    match runProgram foreground with
    | Terminated -> failwith "program terminated"
    | Running _ -> failwith "Should not return until program is waiting"
    | Waiting p -> 
            let output = p.output |> List.rev
            let nextForeground = { background with input = Queue.enqueueSeq output background.input }
            let nextBackground = { p with output = []}
            if isBlocked nextBackground && isBlocked nextForeground 
            then if nextForeground.id = 1 then nextForeground else nextBackground
            else run nextForeground nextBackground


let initialState programId instructions = 
    {
        id = programId
        registers = Map.add 'p' (int64 programId) Map.empty; 
        instructions = instructions; currentInstruction = 0; 
        sentCount = 0; 
        input = Queue.empty;
        output = List.empty }


let part2 =
    System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
    let instructions = System.IO.File.ReadAllLines("input.txt") |> Array.map parseInstruction
    let program0 = initialState 0 instructions
    let program1 = initialState 1 instructions
    run program0 program1

////////////////////////////////////////////////////////////////////////////////////////
// Tests

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let sampleInstructions = 
    [|
        "snd 1"
        "snd 2"
        "snd p"
        "rcv a"
        "rcv b"
        "rcv c"
        "rcv d"
    |]

let tests = 
    testList "Example" [
        testCase "Part 2" <| fun _ ->
            let instructions = sampleInstructions |> Array.map parseInstruction
            let program0 = initialState 0 instructions
            let program1 = initialState 1 instructions

            let result = run program0 program1
            Expect.equal result.id 1 "Expected three sends"
            Expect.equal result.sentCount 3 "Expected three sends"
    ]

tests |> runTests defaultConfig