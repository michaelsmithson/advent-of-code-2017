System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let updateInstruction1 value = value + 1
let updateInstruction2 value = if value >= 3 then value - 1 else value + 1

let steps input updateInstruction = 
        let copy = Array.copy input
        let length = Array.length copy
        let rec loop current count =
            if current < 0 || current >= length then count
            else
                let jump = copy.[current]
                let next = current + jump
                copy.[current] <- updateInstruction jump
                loop next (count + 1) 
        loop 0 0

let input = System.IO.File.ReadAllLines "input.txt" |> Array.map int

let part1 = steps input updateInstruction1

let part2 = steps input updateInstruction2


let tests = testList "Sample" [
    testCase "Sample 1" <| fun _ ->
        let sample = [|0;3;0;1;-3;|]
        let result = steps sample updateInstruction1
        Expect.equal result 5 "5 steps"
    testCase "Sample 2" <| fun _ ->
        let sample = [|0;3;0;1;-3;|]
        let result = steps sample updateInstruction2
        Expect.equal result 10 "5 steps"
]

tests |> runTests defaultConfig