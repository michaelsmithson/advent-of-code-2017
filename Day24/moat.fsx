type Component = { port1: int; port2: int }
type Bridge = List<Component>
type State = {bridge: Bridge; used: Set<Component>}
type BuildState = | Complete of State | Building of State list

let strength = List.fold (fun s c -> s + c.port1 + c.port2) 0
let compatible a b = a.port2 = b.port1 || a.port2 = b.port2
let flip c = {port1 = c.port2; port2 = c.port1}
let pitFace = {port1 = 0; port2 = 0}
let bridgeEnd b = match b with | [] -> pitFace | h::_ -> h

let addComponent state c =
    let current = bridgeEnd state.bridge
    let orientated = if current.port2 = c.port1 then c else flip c
    { bridge = orientated::state.bridge; used = Set.add c state.used }

let buildNext state available = 
    let current = bridgeEnd state.bridge
    let possible = available |> List.filter (fun x -> compatible current x && not <| Set.contains x state.used)
    match possible with
    | [] -> Complete state
    | x -> x |> List.map (addComponent state) |> Building

let parseComponent (s:string) = 
    let tokens = s.Split([|'/'|]) |> Array.map int |> Array.sort
    {port1 = tokens.[0]; port2 = tokens.[1]}

let strongestBridge a b = if strength a > strength b then a else b

let longestBridge a b = 
    let lengthA = List.length a
    let lengthB = List.length b
    if lengthA = lengthB then strongestBridge a b
    elif lengthA < lengthB then b
    else a

let solve bridgeComparer components =
    let rec loop toSearch best =
        match toSearch with
        | [] -> best
        | h::t ->
            match buildNext h components with
            | Complete state -> loop t (bridgeComparer best state.bridge)
            | Building next -> loop (next@t) best
    loop [{bridge = []; used = Set.empty}] []
    |> strength
                
let solve1 = solve strongestBridge
let solve2 = solve longestBridge


System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
let components = System.IO.File.ReadAllLines("input.txt") |> Array.map parseComponent |> List.ofArray
let part1 = solve1 components
let part2 = solve2 components


/////////////////////////////////////////////////////////////////
// Tests

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let sampleComponents = 
        [
            "0/2"
            "2/2"
            "2/3"
            "3/4"
            "3/5"
            "0/1"
            "10/1"
            "9/10"
        ] |> List.map parseComponent

let tests = 
    testList "Sample" [
        testCase "Part 1" <| fun _ ->
            let result = solve1 sampleComponents
            Expect.equal result 31 ""
        testCase "Part 2" <| fun _ ->
            let result = solve2 sampleComponents
            Expect.equal result 19 ""
    ]

tests |> runTests defaultConfig