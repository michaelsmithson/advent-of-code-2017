
let rec skipGarbage input =
        match input with
        | [] -> []
        | '!'::_::t -> skipGarbage t
        | '>'::t -> t
        | _::t -> skipGarbage t

let rec scoreGroup input nest score = 
        match input with
        | [] -> score
        | '!'::_::t -> scoreGroup t nest score
        | '<'::t -> scoreGroup (skipGarbage t) nest score 
        | '{'::t -> scoreGroup t (nest + 1) score
        | '}'::t -> scoreGroup t (nest - 1) (score + nest)
        | _::t -> scoreGroup t nest score

let rec skipGroup input =
    match input with
    | [] -> []
    | '!'::_::t -> skipGroup t
    | '<'::t -> t
    | _::t -> skipGroup t

let countGarbage input =
    let rec loop input count = 
        match input with
        | [] -> count
        | '!'::_::t -> loop t count
        | '>'::t -> loop (skipGroup t) count
        | _::t -> loop t (count + 1)
    // ensure we start the first loop at the first bit of garbage
    let initial = 
        match input with
        | '<'::t -> t
        | _ -> skipGroup input
    loop initial 0

let strToCharList (s:string) = s.ToCharArray() |> List.ofArray

System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
let input = strToCharList <| System.IO.File.ReadAllLines("input.txt").[0]
let part1 = scoreGroup input 0 0

let part2 = countGarbage input

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let createPart1Test (expected,input) = 
    testCase input <| fun _ ->
        let result = scoreGroup (strToCharList input) 0 0
        Expect.equal result expected ""

let createPart2Test (expected,input) = 
    testCase input <| fun _ ->
        let result = countGarbage (strToCharList input)
        Expect.equal result expected ""

let tests = testList "All Tests" [
                [
                    (1, "{}")
                    (6, "{{{}}}")
                    (5, "{{},{}}")
                    (16, "{{{},{},{{}}}}")
                    (1, "{<a>,<a>,<a>,<a>}")
                    (9, "{{<ab>},{<ab>},{<ab>},{<ab>}}")
                    (9, "{{<!!>},{<!!>},{<!!>},{<!!>}}")
                    (3, "{{<a!>},{<a!>},{<a!>},{<ab>}}")
                ] 
                |> List.map createPart1Test
                |> testList "Part 1" 
                [
                    (0, "<>")
                    (17, "<random characters>")
                    (3, "<<<<>")
                    (2, "<{!>}>")
                    (0, "<!!>")
                    (0, "<!!!>>")
                    (10, "<{o\"i!a,<{i<a>")
                ] 
                |> List.map createPart2Test
                |> testList "Part 2" 
    ]


tests |> runTests defaultConfig