
type Move =
    | Spin of int
    | Exchange of int * int
    | Partner of char * char

let parseMove (move:string) =
    let moveType = move.[0]
    let details = move.Substring(1)
    match moveType with
    | 's' -> Spin (int details)
    | 'x' ->
            let positions = details.Split('/') |> Array.map int
            Exchange (positions.[0], positions.[1])
    | 'p' ->
            let partners = details.ToLowerInvariant().Split('/')
            Partner (partners.[0].[0], partners.[1].[0])
    | _ -> failwith "unsupported move type"

let parseMoves (input:string) = input.Split(',') |> Array.map parseMove

let rotate i a = 
    let n = Array.length a
    [|for k in 0..n - 1 ->
            let k = (k + i) % n
            a.[if k < 0 then n + k else k] |]

let applyMove programs move = 
    match move with
    | Spin x -> rotate -x programs
    | Exchange (a,b) ->
        let temp = programs.[b] 
        programs.[b] <- programs.[a]
        programs.[a] <- temp
        programs
    | Partner (pa,pb) ->
        let a = programs |> Array.findIndex ((=) pa)
        let b = programs |> Array.findIndex ((=) pb)        
        let temp = programs.[b] 
        programs.[b] <- programs.[a]
        programs.[a] <- temp
        programs

let dance programs = Seq.fold applyMove (Array.copy programs)

let solve1 (programs:string) input =
    input
    |> parseMoves
    |> dance (programs.ToCharArray())
    |> System.String.Concat

let arrayEqual a b = Array.forall2 (=) a b

let iterationsUntilRepeat initialPrograms moves = 
    let rec loop i programs =
        if i > 0 && arrayEqual programs initialPrograms then i
        else loop (i + 1) (dance programs moves)
    loop 0 initialPrograms

let applyMoves initialPrograms moves count =
    let rec loop i programs = 
        if i >= count then programs
        else loop (i + 1) (dance programs moves)
    loop 0 initialPrograms

let solve2 (programs:string) input =
    let moves = parseMoves input
    let initial = programs.ToCharArray()
    let n = iterationsUntilRepeat initial moves
    let m = 1000000000 % n
    applyMoves initial moves m
    |> System.String.Concat

System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
let input = System.IO.File.ReadAllText("input.txt")

let programs = [|'a'..'p'|] |> System.String.Concat
let part1 = solve1 programs input

let part2 = solve2 programs input


////////////////////////////////////////////////////////////////////////////////////////
// Tests

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let part1Tests = 
    testList "Part 1" [
        testCase "Spin" <| fun _ ->
            let result = solve1 "abcde" "s1"
            Expect.equal result "eabcd" ""
        testCase "Exchange" <| fun _ ->
            let result = solve1 "eabcd" "x3/4"
            Expect.equal result "eabdc" ""
        testCase "Partner" <| fun _ ->
            let result = solve1 "eabdc" "pe/b" 
            Expect.equal result "baedc" ""
        testCase "Example 1" <| fun _ ->
            let result = solve1 "abcde" "s1,x3/4,pe/b"  
            Expect.equal result "baedc" ""
    ]

let part2Tests = 
    testList "Part 2" [
        testCase "Find Repeat" <| fun _ ->
            let moves = parseMoves "s1,x3/4,pe/b"
            let result = iterationsUntilRepeat  ("abcde".ToCharArray()) moves
            Expect.equal result 4 ""

    ]

let tests = testList "All tests" [part1Tests; part2Tests]

tests |> runTests defaultConfig
