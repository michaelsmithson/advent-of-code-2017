System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
let grid = System.IO.File.ReadAllLines("input.txt") |> Array.map (fun x -> x.ToCharArray())

type Direction = | Up | Down | Left| Right
type Cell = { x:int; y: int }
type Position = { cell: Cell; direction: Direction}

let right cell = {cell with x = cell.x + 1 }
let left cell = {cell with x = cell.x - 1 }
let up cell = {cell with y = cell.y - 1 }
let down cell = {cell with y = cell.y + 1 }

let start = { cell = {y = 0; x = grid.[0] |> Array.findIndex ((=) '|')}; direction = Down }
let cell c = grid.[c.y].[c.x]

let next position = 
    match position.direction with
    | Up -> {position with cell = up position.cell}
    | Down -> {position with cell = down position.cell}
    | Left -> {position with cell = left position.cell}
    | Right -> {position with cell = right position.cell}

let change position =
    match position.direction with 
    | Down | Up -> 
            if position.cell |> right |> cell <> ' ' 
            then { position with direction = Right; cell = right position.cell } 
            else { position with direction = Left; cell = left position.cell } 
    | Left | Right -> 
            if position.cell |> down |> cell <> ' ' 
            then { position with direction = Down; cell = down position.cell } 
            else { position with direction = Up; cell = up position.cell } 

let rec follow position letters count =
    match cell position.cell with
    | ' ' -> letters |> List.rev, count // reached the end
    | '+' -> follow (change position) letters (count + 1)
    | '-' | '|' -> follow (next position) letters  (count + 1)
    | c -> follow (next position) (c::letters)  (count + 1)

let letters, count = follow start [] 0
let word = System.String.Concat letters