
type State = { buffer: int array; position: int }

let currentValue state = Array.item state.position state.buffer

let insert index value input =
    let firstPart = Array.take index input
    let endPart = Array.skip index input
    Array.concat [| firstPart; [|value|]; endPart |]

let insertNext steps state  = 
        let length = Array.length state.buffer
        let nextValue = 1 + currentValue state
        let insertionPoint = ((state.position + steps) % length) + 1
        { buffer = insert insertionPoint nextValue state.buffer; position = insertionPoint  }

let solve1 steps iterations = 
    let rec loop i state =
        if i >= iterations then state
        else loop (i + 1) (insertNext steps state)
    let final = loop 0 {position = 0; buffer = [|0|]}
    let targetPosition = final.position + 1
    if targetPosition >= Array.length final.buffer then Array.head final.buffer
    else Array.item targetPosition final.buffer

let solve2 steps =
    let insertionPoint position length = ((position + steps) % length) + 1
    let rec loop i position afterZero = 
        if i = 50000000 then afterZero
        else
            let nextPosition = insertionPoint position i
            loop (i + 1) nextPosition (if nextPosition = 1 then i else afterZero)
    loop 1 0 0

let part1 = solve1 354 2017

let part2 = solve2 354

////////////////////////////////////////////////////////////////////////////////////////
// Tests

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let insertionTest (steps,initial,expected) = 
    testCase (sprintf "step %i buffer %A" steps initial.buffer) <| fun _ ->
        let result = insertNext 3 initial
        Expect.equal result expected ""


let insertionTests =
    [
        (3, {buffer = [|0|]; position = 0}, {buffer = [|0; 1|]; position = 1})
        (3, {buffer = [|0;1|]; position = 1}, {buffer = [|0; 2; 1|]; position = 1})
        (3, {buffer = [|0;2;1|]; position = 1}, {buffer = [|0; 2; 3; 1|]; position = 2})
        (3, {buffer = [|0;2;3;1|]; position = 2}, {buffer = [|0; 2; 4; 3; 1|]; position = 2})
        (3, {buffer = [|0; 2; 4; 3; 1|]; position = 2}, {buffer = [|0; 5; 2; 4; 3; 1|]; position = 1})
    ]
    |> List.map insertionTest
    |> testList "Insertion Tests"


let tests = testList "All tests" [insertionTests;]

tests |> runTests defaultConfig
