
let next multiplier prev = (multiplier * prev) % (int64 System.Int32.MaxValue)

let generatorA = next 16807L
let generatorB = next 48271L 

let rec sequence generator divisor prev = 
    seq {
        let next = generator prev
        if (next % divisor) = 0L then yield next
        yield! sequence generator divisor next
    }

let sequenceA = sequence generatorA
let sequenceB = sequence generatorB

let m = System.Math.Pow (2., 16.) |> int64
let isMatch a b = (a % m) = (b % m)

let countMatches sequenceA sequenceB count =
    Seq.map2 isMatch sequenceA sequenceB 
    |> Seq.take count
    |> Seq.filter id
    |> Seq.length

let judgedRoundsPart1 = 40000000
let judgedRoundsPart2 = 5000000

let part1 = countMatches (sequenceA 1L 289L) (sequenceB 1L 629L) judgedRoundsPart1
let part2 = countMatches (sequenceA 4L 289L) (sequenceB 8L 629L) judgedRoundsPart2

////////////////////////////////////////////////////////////////////////////////////////
// Tests

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let matchTest (a,b,expected) =
    testCase (sprintf "%i" a) <| fun _ ->
        let result = isMatch a b
        Expect.equal result expected ""

let matchTests =
    [
        (1092455L,430625591L, false)
        (245556042L,1431495498L, true)
    ]
    |> List.map matchTest
    |> testList "Match tests"

let sequenceTests = 
    testList "Sequences" [
        testCase "Sequence A Part 1" <| fun _ ->
            let result = sequenceA 1L 65L |> Seq.take 5
            Expect.sequenceEqual result [1092455L;1181022009L;245556042L;1744312007L;1352636452L] ""
        testCase "Sequence B Part 1" <| fun _ ->
            let result = sequenceB 1L 8921L |> Seq.take 5
            Expect.sequenceEqual result [430625591L;1233683848L;1431495498L;137874439L;285222916L] ""
        testCase "Count matches" <| fun _ ->
            let result = countMatches (sequenceA 1L 65L) (sequenceB 1L 8921L) 5
            Expect.equal result 1 "one match"
        testCase "Sequence A Part 2" <| fun _ ->
            let result = sequenceA 4L 65L |> Seq.take 5
            Expect.sequenceEqual result [1352636452L;1992081072L;530830436L;1980017072L;740335192L] ""
        testCase "Sequence B Part 2" <| fun _ ->
            let result = sequenceB 8L 8921L |> Seq.take 5
            Expect.sequenceEqual result [1233683848L;862516352L;1159784568L;1616057672L;412269392L] ""
    ]

let sampleTests = 
    testList "Examples" [
        testCase "Part 1 Sample" <| fun _ ->
            let result = countMatches (sequenceA 1L 65L) (sequenceB 1L 8921L) judgedRoundsPart1
            Expect.equal result 588 ""
        testCase "Part 2 Sample" <| fun _ ->
            let result = countMatches (sequenceA 4L 65L) (sequenceB 8L 8921L) judgedRoundsPart2
            Expect.equal result 309 ""
    ]

let tests = 
    testList "All tests" [
        sequenceTests
        matchTests
        sampleTests
    ]

tests |> runTests defaultConfig
