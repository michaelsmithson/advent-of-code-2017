type ConditionType = Equal | NotEqual |GreaterThan | LessThan | GreaterThanOrEqual | LessThanOrEqual

type Condition = { conditionType: ConditionType; register: string; value: int}

type OperationType = Increment | Decrement

type Operation = {operationType: OperationType; register: string; value: int}

type Instruction = {operation: Operation; condition: Condition}

let getRegisterValue register registers = 
        match Map.tryFind register registers with
        | None -> 0
        | Some value -> value

let conditionPasses (condition:Condition) registers = 
    let value = getRegisterValue condition.register registers
    let operator = 
        match condition.conditionType with
        | Equal -> (=)
        | NotEqual -> (<>)
        | GreaterThan -> (>)
        | LessThan -> (<)
        | GreaterThanOrEqual -> (>=)
        | LessThanOrEqual -> (<=)
    operator value condition.value

let applyOperation operation (registers, maxValue) = 
    let value = getRegisterValue operation.register registers
    let operator = 
        match operation.operationType with
        | Increment -> (+)
        | Decrement -> (-)
    let updatedValue = operator value operation.value
    let updatedRegisters = Map.add operation.register updatedValue registers
    let updatedMaxValue = max maxValue updatedValue
    updatedRegisters, updatedMaxValue

let applyInstruction (registers, maxValue) instruction =
    if conditionPasses instruction.condition registers 
    then applyOperation instruction.operation (registers, maxValue)
    else (registers, maxValue)

let parseComparison s = 
        match s with
        | ">" -> GreaterThan
        | ">=" -> GreaterThanOrEqual
        | "==" -> Equal
        | "!=" -> NotEqual
        | "<=" -> LessThanOrEqual
        | "<" -> LessThan
        | _ -> failwith "unexpected comparison"

let parseOperation s = 
    match s with
    | "inc" -> Increment
    | "dec" -> Decrement
    | _ -> failwith "unexpected operation"

let parseInstruction (s:string) = 
        // format is well defined, so a simple split seems sufficient for parsing
        let tokens = s.Split([|' '|])
        {
            operation = { operationType = parseOperation tokens.[1]; register = tokens.[0]; value = int(tokens.[2]) };
            condition = { conditionType = parseComparison tokens.[5]; register = tokens.[4]; value = int(tokens.[6]) }
        }

let processInstructions instructions = 
    instructions 
    |> Seq.map parseInstruction
    |> Seq.fold applyInstruction (Map.empty, 0)

let largestRegister registers = registers |> Map.toSeq |> Seq.maxBy snd |> snd


System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
let input = System.IO.File.ReadAllLines("input.txt")


let registers, part2 = input |> processInstructions
let part1 = largestRegister registers


#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let tests = 
    let sample = [
        "b inc 5 if a > 1";
        "a inc 1 if b < 5";
        "c dec -10 if a >= 1";
        "c inc -20 if c == 10";
    ]
    testList "Samples" [
        testCase "Part 1" <| fun _ ->
            let result = processInstructions sample
            let largest = result |> fst |> largestRegister
            Expect.equal largest 1 "Largest value should be 1"
        testCase "Part 2" <| fun _ ->
            let result = processInstructions sample
            let largest = result |> snd
            Expect.equal largest 10 "Largest value should be 10"
    ]

tests |> runTests defaultConfig