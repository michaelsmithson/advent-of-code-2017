
type Direction = | North | South | East | West
type Location = { x:int; y: int }
type Position = { location: Location; direction: Direction}
type Rotation = | Left | Right | None | Reverse
type State = | Clean | Weakened | Infected | Flagged

// create a map of the grid centered about 0,0
let createGrid input = 
    let yLength = (Array.length input)
    let yCenter = yLength / 2
    let xLength = (Array.length input.[0])
    let xCenter = xLength / 2
    [|
        for y in 0..(yLength - 1) do
            for x in 0..(xLength - 1) do
                let state = if input.[y].[x] = '#' then Infected else Clean
                yield ({x = x - xCenter; y = y - yCenter}, state)
    |] |> Map.ofArray
        
let rotateLeft direction = 
    match direction with
    | North -> West
    | West -> South
    | South -> East
    | East -> North

let rotateRight direction = 
    match direction with
    | North -> East
    | East -> South
    | South -> West
    | West -> North

let moveForward direction location = 
    match direction with
    | North -> {location with y = location.y - 1}
    | South -> {location with y = location.y + 1}
    | East -> {location with x = location.x + 1}
    | West -> {location with x = location.x - 1}
    
let move virusCarrier rotation  =
    let direction = 
        match rotation with
        | Left -> rotateLeft virusCarrier.direction
        | Right -> rotateRight virusCarrier.direction
        | None -> virusCarrier.direction
        | Reverse -> (rotateRight >> rotateRight) virusCarrier.direction
    {direction = direction; location = moveForward direction virusCarrier.location}

let nodeState location grid = Map.tryFind location grid |> Option.defaultValue Clean

let determineRotation state = 
    match state with
    | Infected -> Right
    | Clean -> Left
    | Weakened -> None
    | Flagged -> Reverse

let updateVirusCarrier virusCarrier state =  determineRotation state |> move virusCarrier

let startPosition = {location = {x = 0; y = 0}; direction = North}

let solve stateMapper bursts input =
    let rec loop i virusCarrier grid infectionCount = 
        if i = bursts then infectionCount
        else
            let currentState = nodeState virusCarrier.location grid
            let newState = stateMapper currentState
            let newGrid = Map.add virusCarrier.location newState grid
            let newVirus = updateVirusCarrier virusCarrier currentState
            let count = if newState = Infected then infectionCount + 1 else infectionCount
            loop (i + 1) newVirus newGrid count
    let grid = createGrid input
    loop 0 startPosition grid 0


let part1StateMap state =
    match state with
    | Infected -> Clean
    | Clean -> Infected
    | _ -> failwith "part 1 only has infected and clean states"

let part2StateMap state =
    match state with
    | Clean -> Weakened
    | Weakened -> Infected
    | Infected -> Flagged
    | Flagged -> Clean


let solve1 = solve part1StateMap

let solve2 = solve part2StateMap

System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)

let input = System.IO.File.ReadAllLines("input.txt") |> Array.map (fun x -> x.ToCharArray())

let part1 = solve1 10000 input

// TODO performance takes about 30s on my machine
let part2 = solve2 10000000 input

/////////////////////////////////////////////////////////////////
// Tests

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let example1Input = [| "..#"; "#.."; "..." |] |> Array.map (fun s -> s.ToCharArray())

let gridTest (location, grid, expected) =
    testCase (sprintf "%A" location) <| fun _ ->
        let result = nodeState location grid
        Expect.equal result expected ""

let gridTests = 
    let grid = createGrid example1Input
    [
        ({x = 0; y = 0}, grid, Clean)
        ({x = 1; y = -1}, grid, Infected)
        ({x = 0; y = 1}, grid, Clean)
        ({x = -1; y = 0}, grid, Infected)
    ]
    |> List.map gridTest
    |> testList "Grid tests"

let tests = 
    testList "Example Tests" [
        testCase "Part 1 Example" <| fun _ ->
            let result = solve1 10000 example1Input
            Expect.equal result 5587 ""
        testCase "Part 2 Example" <| fun _ ->
            let result = solve2 10000000 example1Input
            Expect.equal result 2511944 ""
        gridTests
    ]


tests |> runTests defaultConfig