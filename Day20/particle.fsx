type Vector = { x: int64; y: int64; z: int64 } with 
            static member (+) (v1,v2) = { x = v1.x + v2.x; y = v1.y + v2.y; z = v1.z + v2.z }
type Particle = { id: int; position: Vector; velocity: Vector; acceleration: Vector }

let updateParticle p = 
    let v = p.velocity + p.acceleration
    { p with position = p.position + v; velocity = v; }

let manhattanDistance p = abs p.x + abs p.y + abs p.z

let parseVector (s:string) =   
    printfn "%s" s
    let tokens = s.Trim([|'<';'>'|]) .Split([|','|])
    {
        x = tokens.[0] |> int64
        y = tokens.[1] |> int64
        z = tokens.[2] |> int64
    }
let parseParticle i s = 
    let matches = System.Text.RegularExpressions.Regex.Matches(s, "<(.*?)>" )
    {
        id           = i
        position     = matches.Item(0).Value |> parseVector
        velocity     = matches.Item(1).Value |> parseVector
        acceleration = matches.Item(2).Value |> parseVector
    }

let simulate times p =
    let rec loop i p =
        if i = times then p
        else loop (i + 1) (updateParticle p)
    loop 0 p

let position p = p.position

let filterCollidedParticles particles =
    particles 
    |> Array.groupBy position
    |> Array.filter (fun (_, p) -> Array.length p = 1)
    |> Array.map (snd >> Array.head)

System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
let input = System.IO.File.ReadAllLines("input.txt") |> Array.mapi parseParticle

let solve1 particles = 
    // do 1000 iterations at a time until it gives consistent results between 1000 items
    let rec loop particles minParticleId =
        let nextItems = Array.map (simulate 1000) particles
        let minParticle = Array.minBy (fun p -> manhattanDistance p.position) nextItems
        if minParticle.id = minParticleId then minParticleId
        else loop nextItems minParticle.id
    loop particles -1
let part1 = solve1 input

let solve2 particles =
    let rec loop i particles lastLength =
        // check every hundred iterationst if lenght is same as 100 iterations ago. If so, assume complete
        if i % 100 = 0 && lastLength = Array.length particles then particles.Length
        else
            let updated = particles |> Array.map (simulate 1) |> filterCollidedParticles 
            loop (i + 1) updated particles.Length
    loop 0 particles 0

let part2 = solve2 input


////////////////////////////////////////////////////////////////////////////////////////
// Tests

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto


let parseTests = 
    testList "Parsing Tests" [
        testCase "ExampleInput" <| fun _ ->
            let result = parseParticle 0 "p=<2366,784,-597>, v=<-12,-41,50>, a=<-5,1,-2>"
            let expected = { id = 0; position = {x = 2366L; y = 784L; z = -597L;}; velocity = {x = -12L; y = -41L; z = 50L;};acceleration = {x = -5L; y = 1L; z = -2L;};}
            Expect.equal result expected ""
    ]

let tests = testList "All tests" [parseTests;]

tests |> runTests defaultConfig
