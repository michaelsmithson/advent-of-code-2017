
let rotate i a = 
    let n = Array.length a
    [|for k in 0..n - 1 ->
            let k = (k + i) % n
            a.[if k < 0 then n + k else k] |]

let reverse input position length =
        let rotated = rotate position input
        let last = rotated |> Array.skip length
        let start = rotated |> Array.take length |> Array.rev
        
        Array.concat [start; last] 
        |> rotate (-position)

let rec processRound lengths skipSize position input =
        let inputLength = Array.length input
        match lengths with
        | [] -> input
        | length::t ->
                let updated = reverse input position length 
                let nextPosition = (position + length + skipSize) % inputLength
                processRound t (skipSize + 1) nextPosition updated

let processRounds count lengths input =
        // since I don't want to change the part 1 algorithm now we have part 2... and we have to 
        // track the position and skipsize, lets just make the length longer
        let multipleRounds = List.concat <| seq {for _ in 1..count do yield lengths}
        processRound multipleRounds 0 0 input

let xorArray a = a |> Array.reduce (^^^)

let hexString (i:int) = i.ToString("X2").ToLowerInvariant()

let generateLengths (input:string) =
        let additionalLengths = [|17;31;73;47;23|]
        let inputLengths = input.ToCharArray() |> Array.map int
        Array.concat [inputLengths; additionalLengths] |> List.ofArray

let sparseHash input = 
        let lengths = generateLengths input
        processRounds 64 lengths [|0..255|]

let denseHash sparse =
        sparse
        |> Array.splitInto 16
        |> Array.map (xorArray >> hexString)
        |> System.String.Concat

let fullHash = sparseHash >> denseHash

let input = "102,255,99,252,200,24,219,57,103,2,226,254,1,0,69,216"

let part1 = 
        let lengths = input.Split([|','|]) |> Array.map int |> List.ofArray
        let knottedArray = processRound lengths 0 0 [|0..255|]
        knottedArray.[0] * knottedArray.[1]
   
let part2 = fullHash input

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let reverseTests = 
        testList "Reverse tests" [
            testCase "Reverse sub list at start of array" <| fun _ ->
                let result = reverse [|0;1;2;3;4|] 0 3
                Expect.sequenceEqual result [|2;1;0;3;4|] ""
            testCase "Reverse sub list not at start of array" <| fun _ ->
                let result = reverse [|0;1;2;3;4|] 1 3
                Expect.sequenceEqual result [|0;3;2;1;4|] ""
            testCase "Reverse sub list that wraps" <| fun _ ->
                let result = reverse [|4;3;0;1;2|] 1 5
                Expect.sequenceEqual result [|3;4;2;1;0|] ""
        ]   

let part1Tests =
        testList "Sample Part 1" [
            testCase "Part 1" <| fun _ ->
                let result = processRound [3;4;1;5] 0 0 [|0..4|]
                Expect.sequenceEqual result [3;4;2;1;0] "Part 1"
        ]

let part2Helpers = 
        testList "Part 2 Tests" [
            testCase "xor array 1" <| fun _ ->
                let result = xorArray [|1;2|]
                Expect.equal result 3 ""
            testCase "xor array 2" <| fun _ ->
                let result = xorArray [|1;2;4|]
                Expect.equal result 7 ""
            testCase "xor array 3" <| fun _ ->
                let result = xorArray [|65;27;9;1;4;3;40;50;91;7;6;0;2;5;68;22|]
                Expect.equal result 64 ""
            testCase "hexString 1" <| fun _ ->
                let result = hexString 64
                Expect.equal result "40" ""
            testCase "hexString 2" <| fun _ ->
                let result = hexString 7
                Expect.equal result "07" ""
            testCase "hexString 3" <| fun _ ->
                let result = hexString 255
                Expect.equal result "ff" ""
            testCase "generateLengths" <| fun _ ->
                let result = generateLengths "1,2,3"
                Expect.sequenceEqual result [49;44;50;44;51;17;31;73;47;23] ""
        ]
let part2SampleTests =
        testList "Part 2 Samples" [
            testCase "empty string" <| fun _ ->
                let result = fullHash ""
                Expect.equal result "a2582a3a0e66e6e86e3812dcb672a272" ""
            testCase "AoC 2017" <| fun _ ->
                let result = fullHash "AoC 2017"
                Expect.equal result "33efeb34ea91902bb2f59c9920caa6cd" ""
            testCase "1,2,3" <| fun _ ->
                let result = fullHash "1,2,3"
                Expect.equal result "3efbe78a8d82f29979031a4aa0b16a9d" ""
            testCase "1,2,4" <| fun _ ->
                let result = fullHash "1,2,4"
                Expect.equal result "63960835bcdc130f0b66d7ff4f6a5a8e" ""
        ]

let tests = testList "All tests" [reverseTests; part1Tests; part2Helpers;]

tests |> runTests defaultConfig