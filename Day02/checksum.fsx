System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto


// Part 1
let rowDifference data = 
    let min = Seq.min data
    let max = Seq.max data
    max - min

let inputRowToIntList (row:string) = row.Trim().Split([|' '; '\t'|]) |> Array.map int |> List.ofArray

let checksum1 (input:string array) = input |> Array.sumBy (inputRowToIntList >> rowDifference)

let input = System.IO.File.ReadAllLines "input.txt"
let part1 = checksum1 input

// Part 2
let findDivisor row = 
    let tryFindDivisor x row = row |> List.filter ((<>) x) |> List.tryFind (fun y -> x % y = 0)
    let rec search remaining =
        match remaining with
        | [] -> failwith "unable to find matching divisor"
        | h::t ->
            match tryFindDivisor h row with
            | None -> search t
            | Some x -> h / x
    search row

let checksum2 (input:string array) = 
            input
            |> List.ofArray
            |> List.sumBy (inputRowToIntList >> findDivisor)

let part2 = checksum2 input

//////////////////////////////////////////////////////////////////////

let tests = 
    testList "Sample Cases" [
        testCase "Part 1" <| fun _ ->
            let testInput = [|"5 1 9 5"; "7 5 3"; "2 4 6 8" |]
            let result = checksum1 testInput
            Expect.equal result 18 "Part 1"
        testCase "Part 2" <| fun _ ->
            let testInput = [|"5 9 2 8"; "9 4 7 3"; "3 8 6 5" |]
            let result = checksum2 testInput
            Expect.equal result 9 "Part 2"
    ]

tests |> runTests defaultConfig