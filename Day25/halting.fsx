
type StateName = A | B | C | D | E | F

type State = {current: StateName; cursor: int; tape: Set<int>}
let currentValueIs1 state = Set.contains state.cursor state.tape
let write1 state = Set.add state.cursor state.tape
let write0 state = Set.remove state.cursor state.tape

let stateA state = 
    if currentValueIs1 state 
    then { current = B; cursor = state.cursor - 1; tape = write0 state }
    else { current = B; cursor = state.cursor + 1; tape = write1 state }

let stateB state = 
    if currentValueIs1 state 
    then { current = E; cursor = state.cursor + 1; tape = write0 state }
    else { current = C; cursor = state.cursor - 1; tape = write1 state }

let stateC state = 
    if currentValueIs1 state 
    then { current = D; cursor = state.cursor - 1; tape = write0 state }
    else { current = E; cursor = state.cursor + 1; tape = write1 state }

let stateD state = { current = A; cursor = state.cursor - 1; tape = write1 state }

let stateE state = 
    if currentValueIs1 state 
    then { current = F; cursor = state.cursor + 1; tape = write0 state }
    else { current = A; cursor = state.cursor + 1; tape = write0 state }

let stateF state = 
    if currentValueIs1 state 
    then { current = A; cursor = state.cursor + 1; tape = write1 state }
    else { current = E; cursor = state.cursor + 1; tape = write1 state }

let step state = 
    state |>
        match state.current with
        | A -> stateA
        | B -> stateB
        | C -> stateC
        | D -> stateD
        | E -> stateE
        | F -> stateF

let solve1 () =
    let rec loop i s = 
        if i = 12861455 then s
        else loop (i + 1) (step s)
    let final = loop 0 {current = A; cursor = 0; tape = Set.empty}
    final.tape.Count

let part1 = solve1 ()