System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let input = System.IO.File.ReadAllLines "input.txt"

let isValid1 (phrase:string) = 
        let words = phrase.Split()
        let uniqueWords = words |> Set.ofArray
        words.Length = uniqueWords.Count

let sortCharacters (s:string) = 
            s.ToCharArray() 
            |> Array.sort 
            |> System.String.Concat

let isValid2 (phrase:string) = 
        let words = phrase.Split() |> Array.map sortCharacters
        let uniqueWords = words |> Set.ofArray
        words.Length = uniqueWords.Count

let validPhrases1 = input |> Seq.filter isValid1 |> Seq.length
let validPhrases2 = input |> Seq.filter isValid2 |> Seq.length

let test phrase valid isValid =
    testCase (sprintf "%s | %b" phrase valid) <| fun _ ->  
        let result = isValid phrase
        Expect.equal result valid ""

let tests = testList "Samples" [
        test "aa bb cc dd ee" true isValid1
        test "aa bb cc dd aa" false isValid1
        test "aa bb cc dd aaa" true isValid1
        test "abcde fghij" true isValid2
        test "abcde xyz ecdab" false isValid2
        test "a ab abc abd abf abj" true isValid2
        test "iiii oiii ooii oooi oooo" true isValid2
        test "oiii ioii iioi iiio" false isValid2
    ]

tests |> runTests defaultConfig