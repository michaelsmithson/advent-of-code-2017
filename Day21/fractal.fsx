type grid = array<array<char>>

module Grid =
    let fromString (s:string) : grid = s.Split([|'/'|]) |> Array.map (fun r -> r.ToCharArray())

    let toString (grid:grid) = grid |> Array.map (System.String.Concat) |> Array.reduce (fun a b -> a + "/" + b)

    let zeroCreate size : grid = [| for _ in 1..size do yield Array.create size '.' |]

    // rotates the grid clockwise 90 degree
    let rotate (g:grid) : grid = 
        let side = Array.length g
        let result = Array.copy g
        let rec column i = 
            if i = side then result
            else
                let newRow = g |> Array.map (Array.item i) |> Array.rev
                result.[i] <- newRow
                column (i + 1)
        column 0

    // flip on vertical axis by reversing each row
    let flip (g:grid) : grid = g |> Array.map Array.rev

    let rotateGridString = fromString >> rotate >> toString

    let flipGridString = fromString >> flip >> toString

    let chunk size (g:grid) = 
        let length = Array.length g
        let count = length / size
        [| 
            for row in 0..(count - 1) do
            yield [|
                for col in 0..(count - 1) do
                let x0 = col * size
                let y0 = row * size
                let c = [|
                    for y in y0 .. (y0 + size - 1) do
                    yield [| for x in x0 .. (x0 + size - 1) do yield g.[y].[x] |] 
                |] 
                yield c |> toString                
            |]  
        |]
    
    let lookupExpansion book grid = 
        let rec loop i grid =
            if i = 9 then failwithf "failed to find mapping for: %s" grid
            elif i = 4 then loop (i + 1) (flipGridString grid)
            else 
                //printfn "seaching %s" grid
                match Map.tryFind grid book with
                | Some x -> x
                | None -> loop (i + 1) (rotateGridString grid)
        loop 0 grid

    let expand book (chunks: string[][]) =
        let chunkMax = Array.length chunks - 1
        [| for y in 0..chunkMax do
            yield [| for x in 0..chunkMax do yield lookupExpansion book chunks.[y].[x] |]
         |]

    let combineX a b =
        let a1 = fromString a
        let b1 = fromString b
        let length = Array.length a1
        [|
            for y in 0..(length - 1) do
                yield Array.append a1.[y] b1.[y]
        |] |> toString
    let combineY a b = a + "/" + b

let parseExpansion (s:string) = 
    let token = s.Replace(" ", "").Split([|"=>"|], System.StringSplitOptions.None)
    token.[0],token.[1]

let createBook lines = lines |> Array.map parseExpansion |> Map.ofArray

let initialPattern = ".#./..#/###"

let enhance book grid = 
    let g = Grid.fromString grid
    let chunkSize = if Array.length g % 2 = 0 then 2 else 3
    g
    |> Grid.chunk chunkSize
    |> Grid.expand book
    |> Array.map (Array.reduce Grid.combineX) 
    |> Array.reduce Grid.combineY

let solve1 iterations book =
    let rec loop i grid = 
        printfn "Iteration %i" i
        if i = iterations then grid
        else loop (i+1) (enhance book grid)
    
    let finalGrid = loop 0 initialPattern
    finalGrid.ToCharArray() |> Array.filter ((=) '#') |> Array.length

System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
let book = createBook <| System.IO.File.ReadAllLines("input.txt")
let part1 = solve1 5 book

// TODO make this nicer ... too much string stuff makes this slow...
let part2 = solve1 18 book

/////////////////////////////////////////////////////////////////
// Tests

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let serialisationTests = 
    testList "Grid tests" [
        testCase "serilsation 3x3" <| fun _ ->
            let expected = ".#./..#/###"
            let result = expected |> Grid.fromString |> Grid.toString
            Expect.equal result expected ""
        testCase "serialisation 2x2" <| fun _ ->
            let expected = ".#/#."
            let result = expected |> Grid.fromString |> Grid.toString
            Expect.equal result expected ""
        testCase "rotation 3x3" <| fun _ ->
            let result = ".#./..#/###" |> Grid.fromString |> Grid.rotate |> Grid.toString
            Expect.equal result "#../#.#/##." ""
        testCase "rotation 2x2" <| fun _ ->
            let result = ".#/##" |> Grid.fromString |> Grid.rotate |> Grid.toString
            Expect.equal result "#./##" ""
        testCase "zeroCreate" <| fun _ ->
            let result = Grid.zeroCreate 2 |> Grid.toString
            Expect.equal result "../.." ""
    ]

let sampleTests = 
    testList "Sample Tests" [
        testCase "Sample 1" <| fun _ ->
            let book =  createBook [|"../.# => ##./#../..."; ".#./..#/### => #..#/..../..../#..#"|] 
            let result = solve1 2 book
            Expect.equal result 12 ""            
    ]

let tests = testList "All tests" [serialisationTests; sampleTests]

tests |> runTests defaultConfig

