
type Direction = N | NE | SE | S | SW | NW

type CubeCoord = {x: int; y: int; z: int}
let Start = {x = 0; y = 0; z = 0}

let distance a b = (abs(a.x - b.x) + abs(a.y - b.y) + abs(a.z - b.z)) / 2
let distanceFromStart = distance Start

let move (coordinate,maxDistance) direction =
    let dx, dy, dz = 
        match direction with
        | N -> 0, 1, -1
        | NE -> 1, 0, -1
        | SE -> 1, -1, 0
        | S -> 0, -1, 1
        | SW -> -1, 0, 1
        | NW -> -1, 1, 0
    let updated = {x = coordinate.x + dx; y = coordinate.y + dy; z = coordinate.z + dz }
    let dist = distanceFromStart updated
    updated, (max dist maxDistance)

let parseDirection d = 
    match d with
    | "ne" -> NE
    | "n" -> N
    | "nw" -> NW
    | "se" -> SE
    | "s" -> S
    | "sw" -> SW
    | _ -> failwith "unexpected path"

let parsePath (path:string) = 
        path.Split([|','|]) 
        |> Array.map parseDirection
        |> List.ofArray

let applyPath = List.fold move (Start,0)

let solve path =
        let final,maxDistance = 
            path
            |> parsePath
            |> applyPath
        let finalDistance = final |> distanceFromStart
        finalDistance, maxDistance

System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
let part1,part2 = solve <| System.IO.File.ReadAllText("input.txt")


/////////////////////////////////////////////////////////////////
// Tests

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let distanceTest (path, expected) = 
    testCase path <| fun _ ->
        let result = solve path |> fst
        Expect.equal result expected ""

let tests =
    [
        ("ne,ne,ne", 3)
        ("ne,ne,sw,sw", 0)
        ("ne,ne,s,s", 2)
        ("se,sw,se,sw,sw", 3)
    ]
    |> List.map distanceTest
    |> testList "Samples" 

tests |> runTests defaultConfig