type Program = { name: string; weight: int; above: string[] }

let parse (s:string) = 
        let tokens = s.Replace(",", "").Split([|' '|])
        let weight = tokens.[1].Trim([|'('; ')'|]) |> int
        let name = tokens.[0]
        let above = 
            if tokens.Length > 3 
            then Array.skip 3 tokens
            else Array.empty
        {name = name; weight = weight; above = above}

let findRoot programs = 
    let parents = programs |> Array.map (fun x -> x.name) |> Set.ofArray
    let children = programs |> Array.collect (fun x -> x.above) |> Set.ofArray
    let root = Set.difference parents children |> Set.toArray
    if root.Length = 1 then root.[0]
    else failwith "Unable to find root"

let solvePart1 rawInput = rawInput |> Array.map parse |> findRoot

// Part 2

type Tree<'NodeData> = 
    | Leaf of 'NodeData
    | Node of 'NodeData * Tree<'NodeData> array

let rec createTree programs program = 
        // we know all programs exist
        let findProgram name = Map.find name programs
        if Array.isEmpty program.above 
        then Leaf (program)
        else 
            let subnodes = program.above |> Array.map (findProgram >> createTree programs)
            Node (program, subnodes)


let programWeight node = 
    match node with
    | Leaf program -> program.weight
    | Node (program, _) -> program.weight

let rec towerWeight node = 
    match node with
    | Leaf program -> program.weight
    | Node (program, above) ->
                let aboveWeight = above |> Array.sumBy towerWeight
                program.weight + aboveWeight

let buildTreeFromInput input = 
    let programList = input |> Array.map parse
    let rootName = findRoot programList
    let programs = programList |> Array.map (fun p -> p.name, p) |> Map.ofArray
    let root = Map.find rootName programs
    createTree programs root

let rec findWeightToBalanceTree node =
        // This function looks at the above programs of the given program to see if any of them are unbalanced
        match node with
        | Leaf _ -> None // a leaf has no above nodes
        | Node (program, above) ->                
                // Get weights of all the above nodes
                // for this puzzle, it seems can ignore case both balanced/unbalanced subtrees can't 
                // be seperated by simple ordering
                let nodeWeights = above |> Array.groupBy towerWeight |> Array.sortBy (snd >> Array.length)
                match nodeWeights.Length with
                | 2 -> 
                        // have ordered by the group length, so the first group should contain the
                        // single unbalanced child
                        let treeWeight, grouped =  nodeWeights.[0]
                        let unbalancedNode = if grouped.Length = 1 then grouped.[0] else failwith "expected 1 unbalanced node"                   
                        
                        match findWeightToBalanceTree unbalancedNode with
                        | None -> 
                                // above nodes are balanced, so this is the one we should change
                                let desiredTreeWeight, _ = nodeWeights.[1]
                                let nodeWeight = programWeight unbalancedNode
                                let desiredNodeWeight = nodeWeight + desiredTreeWeight - treeWeight                                
                                Some (desiredNodeWeight)
                        | x -> x // a child of this child is unbalanced - pass along
                | 1 ->  None // The above programs are balanced
                | _ -> failwith "there should only be 1 unbalanced node"


let solvePart2 input = 
        let tree = buildTreeFromInput input
        match findWeightToBalanceTree tree with
        | None -> failwith "failed to find unbalanced node"
        | Some weight -> weight
                

System.IO.Directory.SetCurrentDirectory (__SOURCE_DIRECTORY__)
let input = System.IO.File.ReadAllLines "input.txt"
let part1 = solvePart1 input
let part2 = solvePart2 input


///////////////////////////////////////////////////////////////////////////////////////////
// Tests

#I "../packages"
#r "Expecto/lib/net461/Expecto.dll"
open Expecto

let testInput = 
            [|
                "pbga (66)"
                "xhth (57)"
                "ebii (61)"
                "havc (66)"
                "ktlj (57)"
                "fwft (72) -> ktlj, cntj, xhth"
                "qoyq (66)"
                "padx (45) -> pbga, havc, qoyq"
                "tknk (41) -> ugml, padx, fwft"
                "jptl (61)"
                "ugml (68) -> gyxo, ebii, jptl"
                "gyxo (61)"
                "cntj (57)"
            |]

let tests = 
    testList "All tests" [
        testCase "Parse Leaf" <| fun _ ->
            let result = parse "jlbcwrl (93)"
            Expect.equal result {name = "jlbcwrl"; weight = 93; above = [||]} "Leaf"
        testCase "Parse Node" <| fun _ ->
            let result = parse "fzqsahw (256) -> lybovx, pdmhva"
            Expect.equal result {name = "fzqsahw"; weight = 256; above = [|"lybovx";"pdmhva"|]} "Leaf"
        testCase "Part 1" <| fun _ ->
            let result = solvePart1 testInput
            Expect.equal result "tknk" ""
        testCase "Part 2" <| fun _ ->
            let result = solvePart2 testInput
            Expect.equal result 60 ""
    ]

tests |> runTests defaultConfig